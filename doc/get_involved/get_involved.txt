\mainpage Get involved

We welcome any feedback (such as bug reports, suggestions for improvement and
other constructive criticism) and would particularly 
love to hear from you if the library has helped you to solve 
any of your problems. 

We also welcome contributions to the library itself, in the form
of
- Additional demo codes/tutorials
- Code that provides additional functionality to the library. 
. 

If you wish to contribute to the library you may with to check 
our <A HREF="../../ToDo/html/index.html">
To-Do List</A> for a list of on-going and future developments. 

The developers can be contacted at
<CENTER> 
<tt> oomph-lib  AT maths DOT man DOT ac DOT uk </tt>
</CENTER> 
\n


 